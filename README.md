# PORTAINER

Simple docker-compose pour utiliser Portainer (monitoring de container)

## USAGE:

```bash
docker-compose up -d
```

Finir la configuration à l'adresse: http://localhost:9000